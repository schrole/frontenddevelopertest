var express = require('express');
var path = require('path');
var app = express();

app.set('views', path.join(__dirname, '../views'));

app.get('/', function (req, res) {
  res.render('index.ejs', { title: 'Vue.js test project' });
});

module.exports = app;