var express = require('express');
var app = express();
var dinosaurList = [
  {"id":1,"name":"Tyrannosaurus","maxLengthInMetres":13,"maxWeightInKgs":14000,"lastUpdated":"2017-01-04T00:00:00"},
  {"id":2,"name":"Triceratops","maxLengthInMetres":9,"maxWeightInKgs":12000,"lastUpdated":"2010-11-29T00:00:00"},
  {"id":3,"name":"Adasaurus","maxLengthInMetres":3,"maxWeightInKgs":70,"lastUpdated":"2013-11-29T00:00:00"},
];

app.get('/dinosaurs', function (req, res) {
  res.send(dinosaurList);
});

app.get('/dinosaur/:id', function (req, res) {
  let dinosaur = dinosaurList.find(d => d.id == req.params.id);
  res.send(dinosaur);
});

module.exports = app;