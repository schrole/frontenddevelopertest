# Front End Developer Test

> A Vue.js project

## Usage

```
# install dependencies
yarn install

# watch client and server at localhost:3000
yarn watch

# run all tests
yarn test

# build and minify client js for production
yarn build-client
```

## Requirements
The API returns JSON string containing a list of dinosaurs with 4 attributes (Name, MaxLengthInMetres, MaxWeightInKgs and LastUpdated) that a user should be able to sort by. A user should be able to sort the list by selecting that attribute from a drop-down list.
An example of the JSON returned is included here: 
```
[
  {"id":1,"name":"Tyrannosaurus","maxLengthInMetres":13,"maxWeightInKgs":14000,"lastUpdated":"2017-01-04T00:00:00"},
  {"id":2,"name":"Triceratops","maxLengthInMetres":9,"maxWeightInKgs":12000,"lastUpdated":"2010-11-29T00:00:00"},
  {"id":3,"name":"Adasaurus","maxLengthInMetres":3,"maxWeightInKgs":70,"lastUpdated":"2013-11-29T00:00:00"},
]
```

The results list and the sorting drop down list should be separate Vue.js components.
Ideally, your solution should be unit tested.

How the results are displayed is up to you but each of the sortable attributes should be readable on screen for each result and the “name” of the dinosaur should link to /dinosaur/{id}

The unit tests should be name as *.spec.js under __tests__ folder which is placed next to your vue page or component.