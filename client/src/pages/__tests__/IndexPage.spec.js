import { mount } from '@vue/test-utils';
import IndexPage from '@/pages/IndexPage.vue';

describe('IndexPage', () => {
  test('should show welcome message', () => {
    const wrapper = mount(IndexPage)
    expect(wrapper.find('#main').text()).toEqual('Welcome to home page!')
  })
})